# ChrismasElfRun

組員分工 紀棋峰-企劃及程式設計跟場地設計 賴韋岑-UI及PPT設計 廖嘉欣-3D建模

## 遊戲玩法介紹

![遊戲玩法](https://gitlab.com/vrclassproject/chrismaselfrun/-/raw/main/Assets/UI/GameStart/IMG_1777.JPG?inline=false)

這遊戲為送聖誕禮物的小精靈，因為有一個小孩表現不好，並沒有分配禮物給該小孩，結果小孩發現了該位發禮物的小精靈

後續便開始追逐，而小精靈在逃跑的過程中需要踩對應顏色的方塊來討跑，藍色方塊按螢幕左半邊，黃色方塊按螢幕右半邊。

