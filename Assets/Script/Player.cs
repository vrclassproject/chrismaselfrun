﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{

    public bool Moved;
    bool greenfloor = false;
    bool yellowfloor = false;
    public GameObject place;
    public GameObject ingame;
    public GameObject gameover;
    int point=0;
    public Text scoretext;
    public Text gameovertext;
    public Text highscoredtext;
    
    public AudioSource getsource;
    float timespeed = 1.1f;
    bool weaddit = false;
    public Animator ani;
    bool gameovered = false;
    
    

    Vector3 Reallypos;
    // Start is called before the first frame update
    void Start()
    {
        scoretext.GetComponent<Text>();
        scoretext.text = point.ToString();
        highscoredtext.text = PlayerPrefs.GetInt("HighScore",0).ToString();
    }
    void Awake()
    {
        Reallypos = transform.position;
        Time.timeScale = 1;
    }
    // Update is called once per frame
    void Update()
    {


        /* if (Input.GetKeyDown("left") && greenfloor == true)
         {
             greenfloor = false;
             gameObject.transform.position = new Vector3(159.4822f, -17.82f, Reallypos.z);
             point++;
             scoretext.text = point.ToString();
             getsource.Play();
             weaddit = false;

         }
         else if (Input.GetKeyDown("right") && greenfloor == true)
         {
             StartCoroutine(GameOver());
         }
         if (Input.GetKeyDown("right") && yellowfloor == true)
         {
             yellowfloor = false;
             gameObject.transform.position = new Vector3(159.4822f, -17.82f, Reallypos.z);
             point++;
             scoretext.text = point.ToString();
             getsource.Play();
             weaddit = false;
         }
         else if (Input.GetKeyDown("left") && yellowfloor == true)
         {
             StartCoroutine(GameOver());
         }
         if (point % 7 == 1&& !weaddit)
         {
             gamespeed();
             weaddit = true;
         }*/
        if (!gameovered)
        {
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);
                if (touch.position.x < Screen.width / 2 && greenfloor == true && Input.touches[0].phase == TouchPhase.Began)
                {
                    greenfloor = false;
                    gameObject.transform.position = new Vector3(159.4822f, -17.82f, Reallypos.z);
                    point++;
                    scoretext.text = point.ToString();
                    getsource.Play();
                    weaddit = false;
                    
                }
                else if (touch.position.x > Screen.width / 2 && greenfloor == true && Input.touches[0].phase == TouchPhase.Began)
                {
                    StartCoroutine(GameOver());
                    gameovered = true;
                }
                if (touch.position.x > Screen.width / 2 && yellowfloor == true && Input.touches[0].phase == TouchPhase.Began)
                {
                    greenfloor = false;
                    gameObject.transform.position = new Vector3(159.4822f, -17.82f, Reallypos.z);
                    point++;
                    scoretext.text = point.ToString();
                    getsource.Play();
                    weaddit = false;
                }
                else if (touch.position.x < Screen.width / 2 && yellowfloor == true && Input.touches[0].phase == TouchPhase.Began)
                {
                    StartCoroutine(GameOver());
                    gameovered = true;
                }
            }
            if (point % 9 == 1 && !weaddit)
            {
                gamespeed();
                weaddit = true;
            }
        }



    }
        void OnTriggerEnter(Collider other)
    {

        if (other.tag == "green")
        {
            Reallypos = other.transform.position;
            greenfloor = true;
            yellowfloor = false;
        }


        if (other.tag == "yellow")
        {
            Reallypos = other.transform.position;
            yellowfloor = true;
            greenfloor = false;
        }


    }
    public void gamestart()
    {
        Time.timeScale = 1;
    }
    IEnumerator GameOver()
    {
        ani.SetBool("gameover", true);
        yield return new WaitForSeconds(2);
        Time.timeScale = 0;
        gameovertext.text = point.ToString();
        if (point > PlayerPrefs.GetInt("HighScore", 0))
        { highscoredtext.text = point.ToString();
        PlayerPrefs.SetInt("HighScore", point); }
        place.SetActive(false);
        ingame.SetActive(false);
        gameover.SetActive(true);



    }
    void gamespeed()
    {
        Time.timeScale = timespeed;
        timespeed = timespeed + 0.1f;
    }
  




}
